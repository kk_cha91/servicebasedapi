require 'rails_helper'

RSpec.describe ProductTerm, type: :model do
	it { is_expected.to be_dynamic_document }
	it { should belong_to(:product) }
	it { should validate_presence_of(:sku) }
	it { should validate_presence_of(:priceDimensions) }
	it { should validate_presence_of(:demandType) }
	it { should validate_presence_of(:effectiveDate) }
end
