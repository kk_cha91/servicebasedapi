require 'rails_helper'

RSpec.describe MainService, type: :model do
	it { is_expected.to be_dynamic_document }
	it { should have_many(:products) }
	it { should validate_presence_of(:offerCode) }
	it { should validate_presence_of(:version) }
end
