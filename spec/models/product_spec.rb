require 'rails_helper'

RSpec.describe Product, type: :model do
	it { is_expected.to be_dynamic_document }
	it { should belong_to(:main_service) }
	it { should have_one(:product_term) }
	it { should validate_presence_of(:product_attributes) }
	it { should validate_presence_of(:sku) }
end
