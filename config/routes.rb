Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/service/:serviceCode/region/:regionName', to: 'main_services#get_product'
end
