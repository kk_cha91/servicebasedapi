require 'rufus-scheduler'

sch1=Rufus::Scheduler.new(:lockfile => ".rufus-scheduler1.lock")
unless defined?(Rails::Console) || File.split($0).last == 'rake' || sch1.down?
	sch1.cron '00 12 * * *' do
		system("rake create_new_record")
	end
end