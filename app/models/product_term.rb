class ProductTerm
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  belongs_to :product
  field :demandType, type: String
  field :sku, type: String
  field :priceDimensions, type: Hash
  field :effectiveDate, type: DateTime
  validates_presence_of :sku, :priceDimensions, :demandType, :effectiveDate
end
