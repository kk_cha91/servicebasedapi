class Product
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  belongs_to :main_service
  has_one :product_term
  field :sku, type: String
  field :product_attributes, type: Hash
  validates_presence_of :sku, :product_attributes
  validates :sku, uniqueness: true
end