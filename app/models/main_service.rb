class MainService
  include Mongoid::Document
   include Mongoid::Attributes::Dynamic
   has_many :products
    field :formatVersion, type: String
    field :disclaimer, type: String
    field :offerCode, type: String
    field :version, type: String
    field :publicationDate, type: DateTime
    validates_presence_of :offerCode, :version

    def self.api
		url = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonCloudFront/current/index.json'
		require 'open-uri'
		require 'json'
		result = JSON.parse open(url).read
		main_service = {}
		result.each do |key, value|
		    if result[key].class != Hash
			    main_service[key] = value
			end
		end
		ms = MainService.where(version: main_service['version'], offerCode: main_service['offerCode']).try(:first)
		ms = ms.present? ? ms.update_attributes(main_service) : MainService.create(main_service)
		if result['products'].present?
	    	result['products'].keys.each do |key|
	    		product_data = {}
	    		result['products'][key].each do |k, v|
	    			k = 'product_attributes' if k == 'attributes'
	    			product_data[k] = v
	    		end
	    		p = Product.where(sku: key).try(:first)
	    		if p.present? 
	    			p.update_attributes(product_data) 
	    			product = p
	    		else
	    			product = ms.products.create(product_data)
	    		end
	    		if result['terms'].present?
			    	demandType = 'OnDemand' if result['terms'].has_key?('OnDemand')
			    	term_key = result['terms']['OnDemand'][product.sku]
			    	term_data = {}
			    	term_data['product_id'] = product.id
			    	term_data['demandType'] = demandType
		    		term_key.keys.each do |k|
		    			term_key[k].each do |kk, vv|
			    			if kk == 'priceDimensions' 
			    				term_key[k][kk].each do |kkk, vvv|
			    					term_data[kk] = vvv
			    				end	
			    			else
			    				term_data[kk] = vv
			    			end
			    		end
			    		t = ProductTerm.where(sku: product.sku, demandType: 'OnDemand').try(:first)
			    		t.present? ? t.update_attributes(term_data) : ProductTerm.create(term_data)
		    		end
			    end
	    	end
	    end
	end
end