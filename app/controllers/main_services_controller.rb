class MainServicesController < ApplicationController
	# API to view pricing for a specific region
	def get_product
		begin
			region = params[:regionName]
			serviceCode = params[:serviceCode]
			ms = MainService.where(offerCode: serviceCode).first
			@p = Product.where('product_attributes.servicecode' => ms.offerCode, 'product_attributes.location' => region ).pluck(:sku)
			if params[:date]
				@pt = ProductTerm.where(:effectiveDate => params[:date]).in(:sku => @p)
			else
				@pt = ProductTerm.in(:sku => @p)
			end
		rescue
			render json: {message: 'Some error occured while fetching data.'}
		end
	end
end
