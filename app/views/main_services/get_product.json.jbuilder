json.array! (@pt.to_a).each do |pt|
	json.description pt.priceDimensions["description"]
	json.beginRange pt.priceDimensions["beginRange"]
	json.endRange pt.priceDimensions["endRange"]
	json.unit pt.priceDimensions["unit"]
	json.pricePerUnit pt.priceDimensions["pricePerUnit"]["USD"]  
	json.effectiveDate pt.effectiveDate
end
